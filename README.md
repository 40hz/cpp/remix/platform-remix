# Remix PlatformIO Platform

This is the [PlatformIO platform](https://docs.platformio.org/en/latest/platforms/index.html)
component for Remix. It provides common build and setup steps and defines the supporting frameworks
and tools. It is configured to work with the Remix framework, and contains a set of predefined
boards.

## Status

Like the rest of Remix, the platform is highly experimental and all parts are subject to change.

Currently, ST Nucleo boards are the main focus, and while some initial code exists for other
targets such as MSP430 or AVR, these are not functional or tested yet.
