#include <algorithm>
#include <cstdint>

extern "C" {

int main();

typedef void (*function_t)();

void resetHandler()
{
    extern std::uint8_t __data_start__;
    extern std::uint8_t __data_end__;
    extern std::uint8_t __etext;
    std::size_t size = static_cast<size_t>(&__data_end__ - &__data_start__);
    std::copy(&__etext, &__etext + size, &__data_start__);

    // Initialize bss section
    extern std::uint8_t __bss_start__;
    extern std::uint8_t __bss_end__;
    std::fill(&__bss_start__, &__bss_end__, 0x00);

    // Initialize static objects by calling their constructors
    extern function_t __init_array_start;
    extern function_t __init_array_end;
    std::for_each(&__init_array_start, &__init_array_end, [](const function_t pfn) { pfn(); });

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
    main();
#pragma GCC diagnostic pop

    while (true)
        ;
}

void defaultHandler()
{
    while (true)
        ;
};

void __attribute__((weak)) nmiHandler() { defaultHandler(); }
void __attribute__((weak)) hardFaultHandler() { defaultHandler(); }
void __attribute__((weak)) memManageHandler() { defaultHandler(); }
void __attribute__((weak)) busFaultHandler() { defaultHandler(); }
void __attribute__((weak)) usageFaultHandler() { defaultHandler(); }
void __attribute__((weak)) svcHandler() { defaultHandler(); }
void __attribute__((weak)) debugMonHandler() { defaultHandler(); }
void __attribute__((weak)) pendSVHandler() { defaultHandler(); }
void __attribute__((weak)) sysTickHandler() { defaultHandler(); }

function_t coreVectors[15] __attribute__((section(".core_vectors"))) = {
    resetHandler,
    nmiHandler,
    hardFaultHandler,
    memManageHandler,
    busFaultHandler,
    usageFaultHandler,
    0,
    0,
    0,
    0,
    svcHandler,
    debugMonHandler,
    0,
    pendSVHandler,
    sysTickHandler,
};

void _close_r() { }
void _read_r() { }
void _write_r() { }
void _lseek_r() { }
}
