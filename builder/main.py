import sys
import os
from platform import system
from platformio.public import list_serial_ports
from os import makedirs
from os.path import join, isdir
from string import Template

from SCons.Script import (COMMAND_LINE_TARGETS, AlwaysBuild, Builder, Default,
                          DefaultEnvironment)

memory_template_arm = Template("""
MEMORY{
    rom(rx): ORIGIN = ${rom_start}, LENGTH = ${rom_size}
    ram(rw): ORIGIN = ${ram_start}, LENGTH = ${ram_size}
}
""")

memory_template_msp430 = Template(
"""
INCLUDE ${mcu}.ld

SECTIONS {
    .reset_vector : {
        SHORT(_start)
    } > RESETVEC
}

__data_start  = __datastart;
__data_size   = __romdatacopysize;
__data_source = __romdatastart;
__bss_start   = __bssstart;
__bss_size    = __bsssize;
__stack       = ${stack_size};
""")

memory_template_avr = Template(
"""
__flash       = ${rom_start};
__flash_size  = ${rom_size};
__ram         = ${ram_start};
__ram_size    = ${ram_size};
__stack_size  = ${stack_size};

INCLUDE picolibc_avr${link_variant}.ld
""")

def to_define(value):
    return value.upper().translate({45: 95})

env = DefaultEnvironment()
platform = env.PioPlatform()
board = env.BoardConfig()

board_id = board.get("id", "")
board_arch = board.get("build.arch", "")
board_family = board.get("build.family", "")
board_mcu = board.get("build.mcu", "")
board_cpu = board.get("build.cpu", "")
board_target = board.get("build.target", "")

cpppath = []
extra_flags = []
link_flags = []
spec_flags = []

for name, package in platform.packages.items():
    if package["optional"]:
        continue
    if name == "framework-cmsis":
        cpppath.append(os.path.join(platform.get_package_dir(name), "CMSIS", "Core", "Include"))
        env.Replace(LDSCRIPT_PATH=os.path.join(
            platform.get_dir(), "builder", "arch", "arm", "linker.ld"))
    elif name.startswith("framework-cmsis-"):
        cpppath.append(os.path.join(platform.get_package_dir(name), "Include"))
    elif name == "toolchain-gccmsp430":
        cpppath.append(os.path.join(
            platform.get_package_dir(name), "msp430-elf", "include", "msp430"))
        env.Append(LINKFLAGS="-L" + os.path.join(
            platform.get_package_dir(name), "msp430-elf", "include", "msp430"))
    if name.startswith("toolchain-"):
        toolchain_path = platform.get_package_dir(name)
        env["ENV"]["GCC_EXEC_PREFIX"] = os.path.join(toolchain_path, "lib", "gcc") + os.sep
        env.Append(LINKFLAGS="-T" + os.path.join(env.subst("$BUILD_DIR"), "memory.ld"))
        cpppath.append(os.path.join(toolchain_path, "msp430-elf", "include", "c++", "13.2.0"))

env.Append(CPPPATH=cpppath)

arch_src_path = os.path.join(platform.get_dir(), os.path.join("builder", "arch", board_arch))
build_path = os.path.join("$BUILD_DIR", "platform-remix")
env.BuildSources(build_path, arch_src_path)

remix_defines = [
    '-DREMIX_ARCH_' + to_define(board_arch),
    '-DREMIX_FAMILY_' + to_define(board_family),
    '-DREMIX_MCU_' + to_define(board_mcu),
    '-DREMIX_CPU_' + to_define(board_cpu),
    '-DREMIX_BOARD_' + to_define(board.id),
    ]

if board_arch == 'arm':
    env.Replace(
        AR="arm-none-eabi-gcc-ar",
        AS="arm-none-eabi-as",
        CC="arm-none-eabi-gcc",
        CXX="arm-none-eabi-g++",
        GDB="arm-none-eabi-gdb",
        OBJCOPY="arm-none-eabi-objcopy",
        RANLIB="arm-none-eabi-gcc-ranlib",
        SIZETOOL="arm-none-eabi-size",
    )
    machine_flags = [
        '-mthumb',
        '-mcpu=' + board_cpu,
    ]
    spec_flags = [
        "--specs=nano.specs",
        "--specs=nosys.specs",
    ]
    remix_defines.append('-DREMIX_MCU_FAMILY_' + to_define(board_mcu[0:7]))
    with open(os.path.join(env.subst("$BUILD_DIR"), "memory.ld"), "w") as f:
        f.write(memory_template_arm.substitute(
            rom_start=0x08000000,
            ram_start=0x20000000,
            rom_size=board.get("upload.maximum_size"),
            ram_size=board.get("upload.maximum_ram_size")))
elif board_arch == 'msp430':
    env.Replace(
        AR="msp430-elf-ar",
        AS="msp430-elf-as",
        CC="msp430-elf-gcc",
        CXX="msp430-elf-g++",
        OBJCOPY="msp430-elf-objcopy",
        RANLIB="msp430-elf-ranlib",
        SIZETOOL="msp430-elf-size",
        UPLOADER="mspdebug",
        UPLOADCMD='$UPLOADER $UPLOAD_PROTOCOL "prog $SOURCES"',
    )
    machine_flags = [
        '-mmcu=' + board_mcu,
    ]
    spec_flags = [
        "--specs=picolibc.specs",
    ]
    with open(os.path.join(env.subst("$BUILD_DIR"), "memory.ld"), "w") as f:
        f.write(memory_template_msp430.substitute(
            rom_start=board.get("memory.rom_start"),
            ram_start=board.get("memory.ram_start"),
            stack_size=board.get("memory.stack_size"),
            mcu=board_mcu,
            rom_size=board.get("upload.maximum_size"),
            ram_size=board.get("upload.maximum_ram_size")))
elif board_arch == 'avr':
    env.Replace(
        AR="avr-ar",
        AS="avr-as",
        CC="avr-gcc",
        CXX="avr-g++",
        OBJCOPY="avr-objcopy",
        RANLIB="avr-ranlib",
        SIZETOOL="avr-size",
    )
    machine_flags = [
        '-mmcu=' + board_family,
    ]
    spec_flags = [
        "--specs=picolibc.specs",
    ]
    extra_flags = [
        "--param=min-pagesize=0",
        "-D__AVR_" + to_define(board_mcu) + "__",
    ]
    if board.get("upload.protocol") == "arduino":
        link_flags += [ "-Wl,--section-start=.init=0x200"]
    if board_family == 'avrxmega2' or board_family == 'avrxmega4':
        link_variant = '_flmap'
    elif board_family == 'avrxmega3':
        link_variant = '_combined'
    else:
        link_variant = ''
    with open(os.path.join(env.subst("$BUILD_DIR"), "memory.ld"), "w") as f:
        f.write(memory_template_avr.substitute(
            link_variant=link_variant,
            rom_start=board.get("memory.rom_start"),
            ram_start=board.get("memory.ram_start"),
            stack_size=board.get("memory.stack_size"),
            mcu=board_mcu,
            rom_size=board.get("upload.maximum_size"),
            ram_size=board.get("upload.maximum_ram_size")))
else:
    sys.stderr.write("Error: Architecture %s not supported.\n" % board_arch)
    env.Exit(1)


env.Replace(
    ARFLAGS=["rc"],

    PIODEBUGFLAGS=["-O0", "-g3", "-ggdb", "-gdwarf-2"],

    SIZEPROGREGEXP=r"^(?:\.text|\.data|\.rodata|\.text.align|\.ARM.exidx)\s+(\d+).*",
    SIZEDATAREGEXP=r"^(?:\.data|\.bss|\.noinit)\s+(\d+).*",
    SIZECHECKCMD="$SIZETOOL -A -d $SOURCES",
    SIZEPRINTCMD='$SIZETOOL -B -d $SOURCES',

    PROGSUFFIX=".elf",
)

env.Append(
    ASFLAGS=machine_flags,
    ASPPFLAGS=[
        "-x", "assembler-with-cpp",
    ],

    CCFLAGS=machine_flags + spec_flags + extra_flags + remix_defines + [
        "-Os",
        "-g",
        "-ffunction-sections",  # place each function in its own section
        "-fdata-sections",
        "-fno-exceptions",
        "-fno-unwind-tables",
        "-Wall",
        "-Wextra",
        "-Wshadow",
        "-Wconversion",
        "-Wpedantic",
    ],

    CXXFLAGS=[
        "-fno-rtti",
        "-fno-exceptions",
        "-fno-threadsafe-statics",
        "-fno-unwind-tables",
        "-std=c++23"
    ],

    CPPDEFINES=[
        ("F_CPU", "$BOARD_F_CPU"),
    ],

    LINKFLAGS=machine_flags + spec_flags + link_flags + [
        "-Os",
        "-Wl,--gc-sections",
        "-lstdc++",
    ],

    LIBS=[""],

    BUILDERS=dict(
        ElfToHex=Builder(
            action=env.VerboseAction(" ".join([
                "$OBJCOPY",
                "-O",
                "ihex",
                "-R",
                ".eeprom",
                "$SOURCES",
                "$TARGET"
            ]), "Building $TARGET"),
            suffix=".hex"
        )
    )
)

# Allow user to override via pre:script
if env.get("PROGNAME", "program") == "program":
    env.Replace(PROGNAME="firmware")

#
# Target: Build executable and linkable firmware
#

target_elf = None
if "nobuild" in COMMAND_LINE_TARGETS:
    target_elf = join("$BUILD_DIR", "${PROGNAME}.elf")
    target_firm = join("$BUILD_DIR", "${PROGNAME}.hex")
else:
    target_elf = env.BuildProgram()
    target_firm = env.ElfToHex(join("$BUILD_DIR", "${PROGNAME}"), target_elf)
    env.Depends(target_firm, "checkprogsize")

AlwaysBuild(env.Alias("nobuild", target_firm))
target_buildprog = env.Alias("buildprog", target_firm, target_firm)

#
# Target: Print binary size
#

target_size = env.Alias(
    "size", target_elf,
    env.VerboseAction("$SIZEPRINTCMD", "Calculating size $SOURCE"))
AlwaysBuild(target_size)

#
# Target: Upload firmware
#

def BeforeArduinoUpload(target, source, env):
    env.AutodetectUploadPort()
    env.Append(UPLOADERFLAGS=["-P", '"$UPLOAD_PORT"'])
    before_ports = list_serial_ports()
    if upload_options.get("use_1200bps_touch", False):
        env.TouchSerialPort("$UPLOAD_PORT", 1200)
    if upload_options.get("wait_for_upload_port", False):
        env.Replace(UPLOAD_PORT=env.WaitForNewSerialPort(before_ports))

upload_protocol = env.subst("$UPLOAD_PROTOCOL")
debug_tools = board.get("debug.tools", {})
upload_source = target_firm
upload_actions = []

if upload_protocol.startswith("jlink"):
    def _jlink_cmd_script(env, source):
        build_dir = env.subst("$BUILD_DIR")
        if not isdir(build_dir):
            makedirs(build_dir)
        script_path = join(build_dir, "upload.jlink")
        commands = [
            "h",
            "loadbin %s, %s" % (source, board.get(
                "upload.offset_address", "0x08000000")),
            "r",
            "q"
        ]
        with open(script_path, "w") as fp:
            fp.write("\n".join(commands))
        return script_path

    env.Replace(
        __jlink_cmd_script=_jlink_cmd_script,
        UPLOADER="JLink.exe" if system() == "Windows" else "JLinkExe",
        UPLOADERFLAGS=[
            "-device", board.get("debug", {}).get("jlink_device"),
            "-speed", env.GetProjectOption("debug_speed", "4000"),
            "-if", ("jtag" if upload_protocol == "jlink-jtag" else "swd"),
            "-autoconnect", "1",
            "-NoGui", "1"
        ],
        UPLOADCMD='$UPLOADER $UPLOADERFLAGS -CommanderScript "${__jlink_cmd_script(__env__, SOURCE)}"'
    )
    upload_actions = [env.VerboseAction("$UPLOADCMD", "Uploading $SOURCE")]

elif upload_protocol == "serial":
    def __configure_upload_port(env):
        return env.subst("$UPLOAD_PORT")

    env.Replace(
        __configure_upload_port=__configure_upload_port,
        UPLOADER=join(
            '"%s"' % platform.get_package_dir("tool-stm32duino") or "",
            "stm32flash", "stm32flash"),
        UPLOADERFLAGS=[
            "-g", board.get("upload.offset_address", "0x08000000"),
            "-b", env.subst("$UPLOAD_SPEED") or "115200", "-w"
        ],
        UPLOADCMD='$UPLOADER $UPLOADERFLAGS "$SOURCE" "${__configure_upload_port(__env__)}"'
    )

    upload_actions = [
        env.VerboseAction(env.AutodetectUploadPort, "Looking for upload port..."),
        env.VerboseAction("$UPLOADCMD", "Uploading $SOURCE")
    ]

elif upload_protocol == "stlink":
    env.Replace(
        UPLOADER=join(
            '"%s"' % platform.get_package_dir("tool-stm32duino") or "",
            "stlink", "st-flash"),
        UPLOADERFLAGS=["--reset", "write"],
        UPLOADERADDRESS=board.get("upload.offset_address", "0x08000000"),
        UPLOADCMD='$UPLOADER $UPLOADERFLAGS "$SOURCE" $UPLOADERADDRESS'
    )
    upload_actions = [env.VerboseAction("$UPLOADCMD", "Uploading $SOURCE")]

elif upload_protocol == "arduino" or upload_protocol == 'pkobn_updi':
    env.Replace(
        UPLOADER="avrdude",
        UPLOADERFLAGS=[
            "-p", "$BOARD_MCU", "-C",
            join(
                env.PioPlatform().get_package_dir("tool-avrdude")
                or "",
                "avrdude.conf",
            ),
            "-c", "$UPLOAD_PROTOCOL"
        ],
        UPLOADCMD="$UPLOADER $UPLOADERFLAGS -U flash:w:$SOURCES:i",
    )

    upload_options = {}
    if "BOARD" in env:
        upload_options = env.BoardConfig().get("upload", {})
    if "extra_flags" in upload_options:
        env.Append(UPLOADERFLAGS=upload_options.get("extra_flags"))

    if upload_protocol == "pkobn_updi":
        env.Append(UPLOADERFLAGS=["-P", 'usb'])

    upload_actions = [env.VerboseAction("$UPLOADCMD", "Uploading $SOURCE")]
    if upload_protocol == "arduino":
        if env.subst("$UPLOAD_SPEED"):
            env.Append(UPLOADERFLAGS=["-b", "$UPLOAD_SPEED"])
        upload_actions.insert(
                0,
                env.VerboseAction(BeforeArduinoUpload, "Looking for upload port..."))


upload_target = target_firm
target_upload = env.Alias("upload", upload_target, upload_actions)
AlwaysBuild(target_upload)

#
# Default targets
#

Default([target_buildprog, target_size])
